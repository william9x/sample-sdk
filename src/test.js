const ownerOf = require('./main')

async function test() {
    const result = await ownerOf(9483)
    if (result.toString() !== "0xb176b6583C07946113Ef572fC5bF2Ec90A6bD059") {
        console.log("Test failed");
        return
    }
    console.log("Test succeed");
}

test()