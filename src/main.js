const Web3 = require('web3');
const LookiNFT = require('./abi/looki_nft')

const RPC = "https://rpc.ankr.com/eth"
const ContractAddress = "0xffc1131dda0299b804c97c436bc8cfea019e00a0"
const contract = new Web3.eth.contract.Contract(LookiNFT, ContractAddress);
contract.setProvider(RPC)

async function ownerOf(nftID) {
    return contract.methods.ownerOf(nftID).call()
}

module.exports = ownerOf